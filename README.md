# Spring Security & JWT 실습

### 📌 공식 문서

👉 Spring Security : https://docs.spring.io/spring-authorization-server/docs/current/reference/html/getting-started.html

👉 JWT : https://jwt.io/



## 💻 구현 기능

|          기능          |                            Note                            |       API Mapping        |  ex  |
| :--------------------: | :--------------------------------------------------------: | :----------------------: | :--: |
|        회원가입        |               [회원가입](./note/회원가입.md)               | POST /api/v1/users/join  |      |
|     Exception 처리     |          [Exception처리](./note/Exception처리.md)          |                          |      |
| UserRestControllerTest | [UserRestControllerTest](./note/UserRestControllerTest.md) |                          |      |
|  Spring Security 적용  |        [Spring Security](./note/SpringSecurity.md)         |                          |      |
|         로그인         |                  [Login](./note/Login.md)                  | POST /api/v1/users/login |      |

